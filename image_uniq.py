from Tkinter import Tk					 # get base widget set
import os, sys
import shutil
from imagediff import ImageDiffSequencer

USAGE = "Usage: %s <image_dir>" % os.path.basename(__file__)
show_compares = False
image_map = None
debug = False


class ImageUnique(object):
	'''
	Create new directory 'unique' and populate with images from homedir/images
	that are compared and found to be unique.
	'''
	def __init__(self, homedir, print_change_details=False):
		use_default_imagemap_name = True
		assert os.path.isdir(homedir), "not a directory: '%s'" % homedir
		self.imagemap_dir = homedir
		self.unique_dir = os.path.join(homedir, 'unique')
		if not os.path.isdir(self.unique_dir):
			os.makedirs(self.unique_dir)
		self.show_details = print_change_details
		self.sequencer = ImageDiffSequencer(homedir, 
										use_default_imagemap_name, 
										False)
		self.sequencer.add_image_change_listener(self.update_image_change)
		self.sequencer.set_log_details(print_change_details)

	def run(self):
		self.sequencer.run()
		
	def update_image_change(self, image, changes, filepath):
		if (filepath == None or len(filepath) < 1):
			raise Exception("update_image_change(): missing filepath")
		else:
			self.unique_image(filepath)
		if self.show_details:
			print "%d areas changed: %s"  % (len(changes), filepath)

	def update_area_change(self, image, change):
		print "update_area_change()"
		
	def unique_image(self, filepath):
		print "unique_image(filepath=%s)" % filepath
		dest = os.path.join(self.unique_dir, os.path.basename(filepath))
		shutil.copy2(filepath, dest)
		pass
		
			
def errorExit(msg=None):
	msgtxt = '' if msg == None else "%s\n" % msg
	sys.stderr.write("%s%s\n" % (msgtxt, USAGE))
	sys.exit(1)
		
if __name__ == '__main__': 
	homedir = None
	use_default_imagemap_name = True # imagemap.json
	print_change_details = False

	if len(sys.argv) > 2 and sys.argv[2] in ('True', 'true', '1'):
		print_change_details = True
		
	if len(sys.argv) > 1:
		homedir = sys.argv[1]
	else:
		errorExit()

	if not os.path.isdir(homedir):
		errorExit("not a directory: %s" % homedir)
	
	use_default = True
	app = ImageUnique(homedir, print_change_details)
	app.run()
	


