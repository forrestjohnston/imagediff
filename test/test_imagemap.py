# File: test_imagemap.py
import os
import shutil
import imagemap
import unittest

class Test_ImageMap_utils(unittest.TestCase):

    def setUp(self):
        self.testdir = os.path.dirname(os.path.realpath(__file__))
        self.tmpdir = self.testdir + "/tmp/"
        self.emptydir = self.testdir + "/tmp/empty"
        self.noexistdir = self.testdir + "/bogus/blahblah"
        self.single_map_dir = self.testdir + '/data/singlemap'
        self.multi_map_dir = self.testdir + '/data/multimaps'
        self.temp_dirs = (self.tmpdir, self.emptydir)
        if os.path.exists(self.noexistdir):
            shutil.rmtree(self.noexistdir, ignore_errors=True)
        for testdir in (self.temp_dirs):
            if not os.path.exists(testdir):
                os.mkdir(testdir)
            self.assertTrue(os.path.exists(testdir), 
                            "missing dir: '%s': " % testdir)
        
    def tearDown(self):
        for testdir in (self.tmpdir, self.emptydir):
            print "removing %s" % testdir
            shutil.rmtree(testdir, ignore_errors=True)
        
    def test_nonexsitant_directory(self):
        """Test with a non-existant directory"""
        self.assertEquals(len(imagemap.find_imagemap_files(self.noexistdir)), 0,
                          "find_imagemap_files of non-existant dir should be 0")
        
    def test_empty_directory(self):
        """Test with an empty directory - no images, no imagemap"""
        self.assertEquals(len(imagemap.find_imagemap_files(self.emptydir)), 0,
                          "find_imagemap_files of empty dir should be 0")
        
    def test_single_map_directory(self):
        """Test with an empty directory - no images, no imagemap"""
        self.assertEquals(len(imagemap.find_imagemap_files(self.single_map_dir)), 1,
                          "find_imagemap_files of single_map  dir should be 1")
        
    def test_multi_map_directory(self):
        """Test with an empty directory - no images, no imagemap"""
        self.assertEquals(len(imagemap.find_imagemap_files(self.multi_map_dir)), 11,
                          "find_imagemap_files of multimap  dir should be 1")
        pass