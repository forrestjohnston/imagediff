
from appcfg import Const

class Dimension(object):
    def __init__(self, width=-1, height=-1):
        self.width = width
        self.height = height
    def __unicode__(self):
        return "%d, %d" % (self.width, self.height)

class Point(object):
    def __init__(self, x=-1, y=-1):
        self.x = x
        self.y = y
    def __unicode__(self):
        return "%d, %d" % (self.x, self.x)

    
class Rect(object):
    """ Java-Serialized Looks like:
    "shape": {
        "type": "rect",
        "rect": {
          "x": 450,
          "y": 91,
          "width": 158,
          "height": 160
        }
    """
    def __init__(self, x=-1, y=-1, w=-1, h=-1):
        self.x = x
        self.y = y
        self.width = w
        self.height = h
        
    def validate(self):
        assert self.x >= 0, "x not initialized"
        assert self.y >= 0, "y not initialized"
        assert self.width >= 0, "width not initialized"
        assert self.height >= 0, "height not initialized"
        return True
    
    def __unicode__(self):
        return "%d, %d, %d, %d" % (self.x, self.y, self.width, self.height)
    
    @staticmethod
    def from_dict(dictp):
        # assert type(dictp) is dict
        x = int(dictp['x']) if dictp.has_key('x') else -1
        y = int(dictp['y']) if dictp.has_key('y') else -1
        w = int(dictp['width']) if dictp.has_key('width') else -1
        h = int(dictp['height']) if dictp.has_key('height') else -1
        rect = Rect(x, y, w, h)
        rect.coords = ','.join( (str(x), str(y), str(w), str(h)) )
        rect.validate()
        return rect
    
class Circle(object):
    """ Java-Serialized Looks like:
      "shape": {
        "type": "circle",
        "circle": {
          "radius": 87,
          "point": {
            "x": 316,
            "y": 173
          }
        }
      """
    def __init__(self, x=-1, y=-1, radius=-1):
        self.x = x
        self.y = y
        self.radius = radius
        
    def validate(self):
        assert self.x >= 0, "x not initialized"
        assert self.y >= 0, "y not initialized"
        assert self.radius >= 0, "radius not initialized"
        return True
    
    @staticmethod
    def from_dict(dictp):
        # assert type(dictp) is dict
        point = dictp['point']
        x = int(point['x']) if point.has_key('x') else -1
        y = int(point['y']) if point.has_key('y') else -1
        radius = int(dictp['radius']) if dictp.has_key('radius') else -1
        circle = Circle(x, y, radius)
        circle.coords = ','.join( (str(x), str(y), str(radius)) )
        circle.validate()
        return circle
    def __unicode__(self):
        return "%d, %d, %d" % (self.x, self.y, self.radius)

          
class Poly(object):
    """ Java-Serialized Looks like:
      "shape": {
        "type": "poly",
        "poly": {
          "npoints": 6,
          "xpoints": [ 504, 402, 504, 612, 612, 612 ],
          "ypoints": [ 813, 918, 1019, 917, 917, 917 ]
        }
      },
    """
    def __init__(self, xpoints=list(), ypoints=list()):
        self.xpoints = xpoints
        self.ypoints = ypoints
        
    def validate(self):
        assert len(self.xpoints) == len(self.ypoints), \
            "length mismatch: xpoints=%d, ypoints=%d" % \
            (len(self.xpoints), len(self.ypoints))
        return True
    
    def points(self):
        plist = [ Point(self.xpoints[i],
                        self.ypoints[i]) for i in range(len(self.xpoints)) ]
        return plist
    
    @staticmethod
    def from_dict(dictp):
        #assert type(dictp) is dict
        xpoints = dictp['xpoints'] if dictp.has_key('xpoints') else list()
        ypoints = dictp['ypoints'] if dictp.has_key('ypoints') else list()
        npoints = int(dictp['npoints']) if dictp.has_key('npoints') else 0
        if npoints > 0:
            assert len(xpoints) == len(ypoints)
            assert len(xpoints) == npoints
        poly = Poly(xpoints, ypoints)
        coordz = [ "(%d,%d)" % (xpoints[i], 
                                ypoints[i]) 
                  for i in range(len(xpoints)) ]
        poly.coords = ','.join(coordz)
        poly.validate()
        return poly
    
    def __unicode__(self):
        points = self.points()
        strlist = ( "(%s)" % (points[i].x, 
                              points[i].y) for i in range(len(points)) )
        rval = ",".join(strlist)
        return rval

class Shape(object):
    """ Java-Serialized Looks like:
    "shape": {
        "type": "rect",
        "rect": {
          "x": 450,
          "y": 91,
          "width": 158,
          "height": 160
        }
    --- Circle looks like: ---
    "shape": {
        "type": "circle",
        "circle": {
          "radius": 87,
          "point": {
            "x": 316,
            "y": 173
          }
        }
    --- Poly looks like: ---
    "shape": {
        "type": "poly",
        "poly": {
          "npoints": 6,
          "xpoints": [ 504, 402, 504, 612, 612, 612 ],
          "ypoints": [ 813, 918, 1019, 917, 917, 917 ]
        }
      },
    """
    def __init__(self, type, shape):
        self.type   = type
        self.shape  = shape
        self.rect   = shape if type == Const.RECT else None
        self.poly   = shape if type == Const.POLY else None
        self.circle = shape if type == Const.CIRCLE else None

    def __unicode__(self):
        return self.shape.__unicode__()
    