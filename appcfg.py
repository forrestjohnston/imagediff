#!/usr/bin/python
# Filename: appConfig.py

def singleton(cls):
    obj = cls()
    cls.__new__ = lambda cls: obj  # Always return the same object
    return cls

@singleton
class Config(object):
    default_dir = './tests'
    image_extensions = [".jpg", ".png", ".bmp"]
    
@singleton
class Const(object):    
    UNKNOWN     = 'unknown'
    RECT        = 'rect'
    CIRCLE      = 'circle'
    POLY        = 'poly'
    RECTANGLE   = RECT
    POLYGON     = POLY
        
    SHAPE_CHOICES = (
        (RECT,   'Rectangle'),
        (CIRCLE, 'Circle'),
        (POLY,   'Polygon'),
    )
    
    