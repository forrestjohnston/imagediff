import os, sys
import json
# from pprint import pprint	   # Debug: pretty-print JSON
from shapes import Rect, Poly, Circle, Shape

__all__ = [ "ImageMap", "ImageArea" ]
DEFAULT_FILE_EXTENSION = '.json'

class ImageMap:
	def __init__(self, id=-1, name=None, src=None, width=-1, height=-1):
		self.id		= id
		self.name	= name
		self.src	= src
		self.width	= width
		self.height = height
		self.areas	= list()
		self.errors = list()
		
	def add(self, area):
		self.areas.append(area)
		
	def validate(self):
		errors = list()
		#if not self.id or self.id < 0:
		#	 errors.append('id')
		if not self.width or self.width < 0:
			errors.append('width')
		if not self.height or self.height < 0:
			errors.append('height')
		if not self.name or len(self.name) == 0:
			errors.append('name')
		if not self.src:
			errors.append('src')
		if len(errors) > 0:
			self.errors = errors
			# raise Exception, "uninitialized: " + ",".join(errors)
		#else:
		#	 self.imagesrc.validate()
		for area in self.areas:
			if not area.validate():
				self.errors = self.errors + area.errors
		return True
		
	def serialize(self):
		return None # Serializer.serialize(self, lists=('areas','areasEx'))
	
	def area_names(self):
		names = [ area.name for area in self.areas ]
		return names
	
	def area_ids(self):
		indexes = [ area.id for area in self.areas ]
		return indexes
	
	def __str__(self):
		return self.__unicode__()
	
	def __unicode__(self):
		return "id=%d, name=%s, src=%s, width=%d, height=%d, areas:[%s]" % \
			(self.id, self.name, self.src, 
			 self.width, self.height, ",".join([str(i) for i in self.area_ids()]))
		
	@staticmethod
	def from_dict(dictp):
		assert type(dictp) is dict
		id		= int(dictp['id']) if dictp.has_key('id') else -1
		name	= dictp['name'] if dictp.has_key('name') else None
		# src	  = dictp['imagesource'] if dictp.has_key('imagesource') else None
		src		= dictp['src'] if dictp.has_key('src') else None
		width	= int(dictp['width']) if dictp.has_key('width') else -1
		height	= int(dictp['height']) if dictp.has_key('height') else -1
		imgmap	= ImageMap(id, name, src, width, height)
		areas	= dictp['areas']
		if len(areas) > 0:
			for area in areas.itervalues():
				try:
					imagearea = ImageArea.from_dict(area)
					imgmap.add(imagearea)
				except Exception as ex:
					print ex
		return imgmap
	

class ImageArea(object):
	def __init__(self, id=-1, name=None, comp=None, shape=None, 
				 excl=False, enabled=True, device=None):
		self.id			= id
		self.name		= name
		self.comparitor = comp
		self.shape		= shape
		self.exclude	= excl
		self.enabled	= enabled
		self.device		= device
		self.errors		= None
		
	def is_excluded(self):
		return self.exclude == True
		
	def is_enabled(self):
		return self.enabled == True
	
	def __str__(self):
		return self.__unicode__()
	
	def get_bounds(self):
		b = self.shape.get_bounds()
		return b
		
	def __unicode__(self):
		dev	 = self.device.id if self.device else None
		shid = self.shape.type if self.shape else None
		return "id=%d, name=%s, comparitor=%s, shape=%s, exclude=%s, enabled=%s, device=%s" % \
			(self.id, self.name, self.comparitor, shid, self.exclude, self.enabled, dev)
			
	def validate(self):
		errors = list()
		#if not self.id or self.id < 0:
		#	 errors.append('id')
		if not self.name or len(self.name) == 0:
			errors.append('name')
		if len(errors) > 0:
			self.errors = errors
			# raise Exception, "uninitialized: " + ",".join(errors)
		return True
		
	@staticmethod
	def from_dict(dictp):
		#assert type(dictp) is dict
		id		= int(dictp['id']) if dictp.has_key('id') else -1
		name	= dictp['name'] if dictp.has_key('name') else None
		comp	= dictp['comparitor'] if dictp.has_key('comparitor') else None
		shape	= dictp['shape'] if dictp.has_key('shape') else None
		excl	= (dictp.has_key('exclude') and dictp['exclude'] == True)
		enabled = (dictp.has_key('enabled') and dictp['enabled'] == True)
		dev		= dictp['device'] if dictp.has_key('device') else None
		if shape:
			type = shape['type']
			if type == 'rect':
				shape = Shape('rect', Rect.from_dict(shape['rect']))
			elif type == 'poly':
				shape = Shape('poly', Poly.from_dict(shape['poly']))
			elif type == 'circle':
				shape = Shape('circle', Circle.from_dict(shape['circle']))
			else:
				raise Exception("unsupported shape '%s'" % shape)
		imgarea = ImageArea(id, name, comp, shape, excl, enabled, dev)
		return imgarea
	
# Utility functions

def find_imagemap_files(dirpath):
	rval = list()
	if (os.path.isdir(dirpath)):
		for f in os.listdir(dirpath):
			if f.endswith(DEFAULT_FILE_EXTENSION):
				rval.append(f)
	return rval

def read_imagemap(fileobj):
	'''Read imagemap where 'fileobj' is either file or directory.
	if fileobj is directory, $fileobj/imagemap.json will be read.
	'''
	mapfile = os.path.join(fileobj + '\\' + 'imagemap.json') \
		if os.path.isdir(fileobj) else fileobj
	with open(os.path.normpath(mapfile)) as data_file:
		print 'reading %s' % mapfile 
		data = json.load(data_file)
	imagemap = ImageMap.from_dict(data)
	# print '%s\n'%imagemap
	return imagemap

def merge_imagemaps(mergeto, mergefrom):
	"""Add areas found in ImageMap mergefrom to mergeto, and return mergeto,
	effectively merging mergefrom into mergeto.	 This facilitates creating an
	ImageMap of a complex view that may be defined by several ImageMap(s)."""
	assert mergeto.validate()
	assert mergefrom.validate()
	warns = list()
	# we want to raise an exception for duplicate names
	names = [ area.name for area in mergeto.areas ]
	for area in mergefrom.areas:
		if area.name in names:
			# TODO: Compare them and if the same, continue
			# else print detailed differences in exception
			# raise Exception, "duplicate area.name %s" % area.name
			warns.append(area.name)
			sys.stderr.write("skipping duplicate area name: %s\n" % area.name)
		else:
			mergeto.add(area)
	return mergeto
	
def read_imagemaps(dirpath):
	"""Read and merge all .json imagemaps found in dirpath, and return a single
	ImageMap that is the union of all ImageAreas in the view."""
	rval = None
	if os.path.isdir(dirpath):
		for f in os.listdir(dirpath):
			if f[-5:] == ".json":
				imap = read_imagemap(os.path.join(dirpath, f))
				if imap.validate():
					rval = merge_imagemaps(rval, imap) if rval else imap
	return rval

