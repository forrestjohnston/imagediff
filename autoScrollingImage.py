from Tkinter import *
import Image, ImageTk

class scrollingImage(Frame):

     def go(self):
                self.canv.move(self.imgtag,-1,0)
                self.canv.update()
                self.after(100,self.go)
                
     def __init__(self, parent=None):
                Frame.__init__(self, parent)
                self.master.title("AutoScroll Viewer")
                self.pack(expand=YES, fill=BOTH)
                self.canv = Canvas(self, relief=SUNKEN)
                self.canv.config(width=200, height=200)
                self.canv.config(highlightthickness=0)

                sbarV = Scrollbar(self, orient=VERTICAL)
                sbarH = Scrollbar(self, orient=HORIZONTAL)

                sbarV.config(command=self.canv.yview)
                sbarH.config(command=self.canv.xview)

                self.canv.config(yscrollcommand=sbarV.set)
                self.canv.config(xscrollcommand=sbarH.set)

                sbarV.pack(side=RIGHT, fill=Y)
                sbarH.pack(side=BOTTOM, fill=X)

                self.canv.pack(side=LEFT, expand=YES, fill=BOTH)
                self.im=Image.open(r"./test/data/singlemap/images/img0002.jpg")
                width,height=self.im.size
                #self.canv.config(scrollregion=(0,0,width,height))
                self.canv.config(scrollregion=(0,0,300,300))
                self.im2=ImageTk.PhotoImage(self.im)
                x,y=0,0
                self.imgtag=self.canv.create_image(x,y,\
                     anchor="nw",image=self.im2)
                self.go()

scrollingImage().mainloop()