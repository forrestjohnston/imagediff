########################################################################
# SlideShow: a simple photo image slideshow in Python/Tkinter;
# the base feature set coded here can be extended in subclasses;
########################################################################
     
from Tkinter import *
from glob import glob
from tkMessageBox import askyesno
from tkFileDialog import askopenfilename
from PIL import Image, ImageTk
#import random
Width, Height = 960, 540
     
imageTypes = [('JPG files', '.jpg'),    # for file open dialog
              ('Gif files', '.gif'),    # for file open dialog
              ('Ppm files', '.ppm'),    # plus jpg with a Tk patch,
              ('Pgm files', '.pgm'),    # plus bitmaps with BitmapImage
              ('All files', '*')]
     
class SlideShow(Frame):
    def __init__(self, parent=None, picdir='.', msecs=1000, **args):
        Frame.__init__(self, parent, args)
        self.makeWidgets()
        self.pack(expand=YES, fill=BOTH)
        self.opens = picdir
        files = []
        for label, ext in imageTypes[:-1]:
            files = files + glob('%s/*%s' % (picdir, ext))
        self.files = iter(files)
        # self.images = map(lambda x: (x, PhotoImage(file=x)), files)
        self.msecs  = msecs
        self.beep   = 1
        self.drawn  = None

    def makeWidgets(self):
        self.canvas = Canvas(self, bg='white', height=Height, width=Width)
        self.canvas.pack(side=TOP, fill=BOTH, expand=YES)
        self.onoff = Button(self, text='Start', command=self.onStart)
        # we dont say: self.onoff = Button(...).pack(side=LEFT, ...) 
        # because the return value would not be a button but rval of the pack
        self.onoff.pack(side=LEFT, padx=5, pady=5)
        Button(self, text='Open',  command=self.onOpen).pack(side=LEFT, padx=5, pady=5)
        Button(self, text='Beep',  command=self.onBeep).pack(side=LEFT, padx=5, pady=5)
        Button(self, text="OK", command=self.onOK).pack(side=RIGHT, padx=5, pady=5)

        Button(self, text='Quit',  command=self.onQuit).pack(side=RIGHT, padx=5, pady=5)

    def onStart(self):
        self.loop = 1
        self.onoff.config(text='Stop', command=self.onStop)
        self.canvas.config(height=Height, width=Width)
        self.onTimer()

    def onStop(self):
        self.loop = 0
        self.onoff.config(text='Start', command=self.onStart)

    def onOpen(self):
        self.onStop()
        name = askopenfilename(initialdir=self.opens, filetypes=imageTypes)
        if name:
            if self.drawn: self.canvas.delete(self.drawn)
            # img = PhotoImage(file=name)
            image = Image.open(name)
            img = ImageTk.PhotoImage(image)
            
            #self.canvas.config(height=img.size(), width=img.width())
            self.drawn = self.canvas.create_image(2, 2, image=img, anchor=NW)
            self.image = img

    def onOK(self):
        self.onStop()
        self.update()
        print 'Here is where we collect and report RESULTS_TEMPLATE'
        self.quit()

    def onQuit(self):
        self.onStop()
        self.update()
        if askyesno('PyView', 'Really quit now?'):
            self.quit()

    def onBeep(self):
        self.beep = self.beep ^ 1

    def onTimer(self):
        if self.loop:
            self.drawNext()
            self.after(self.msecs, self.onTimer)

    def drawNext(self):
        if self.drawn: self.canvas.delete(self.drawn)
        # name, img  = random.choice(self.images)
        name = self.files.next()
        image = Image.open(name) # new forrest
        image = image.resize((Width, Height), Image.ANTIALIAS) 
        img = ImageTk.PhotoImage(image) # new forrest
        
        self.drawn = self.canvas.create_image(2, 2, image=img, anchor=NW)
        self.image = img
        if self.beep: self.bell()
        self.canvas.update()
     
if __name__ == '__main__':
    import sys
    if len(sys.argv) == 2:
        picdir = sys.argv[1]
    else:
        picdir = '../gifs'
    root = Tk()
    root.title('PyView 1.0')
    root.iconname('PyView')
    Label(root, text="Python Slide Show Viewer").pack()
    SlideShow(root, picdir=picdir, bd=3, relief=SUNKEN)
    root.mainloop()
