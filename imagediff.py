from Tkinter import Tk
import os, sys
import util.fileutil as fileutil
from forrest.utils.imaging import dhash
from forrest.imaging.imagediff import Contour_ImageDiff # Entropy_ImageDiffer
import imagemap
from appcfg import Config
import tkMessageBox
from imageViewer import ImageSequenceViewer
from PIL import Image, ImageDraw
import cv2, numpy # converts PIL to cv2

"""
Usage: imagediff.py viewer C:\forrest\workspace\_sequences\images
Usage: imagediff.py sequencer C:\forrest\workspace\_sequences\images

This application will act both as a imagediff sequencer - with no UI -
or as a imagediff sequencer with a viewer frame and navigation controls.

To customize for another application, here is where you can interface
your application by extending either ImageDiffSequencer or ImageDiffEditor:

ImageDiffSequencer.run() visits all images iteratively processing the 
  sequencer's next() image independent of any UI window.
   
ImageDiffEditor.__init__() uses ImageSequenceViewer to select 
  images interactively

"""
USAGE = "Usage: %s <viewer|sequencer> <image_dir>" % os.path.basename(__file__)
image_map = None
debug = True
			
defaultEntropyThreshold = 1.59
		
		
class ImageChangeProcessor(object):
	'''
	Individual ImageArea changes to update_area_change() while
	list of changes to update_image_change()
	'''
	def __init__(self, savedir=None, show_details=False):
		self.savedir = savedir
		self.show_details = show_details
		self.log_details = False
		self.log_fh = None
		self.all_changes = dict()
		
	def log(self, txt):
		if self.log_fh:
			self.log_fh.write(txt+'\n')
			self.log_fh.flush()
			os.fsync(self.log_fh.fileno())
		else: 
			print txt
	
	def show_details(self, bval):
		self.show_details = bval
		
	def set_log_details(self, bval):
		self.log_details = bval
	

	#from PIL import Image, ImageFont, ImageDraw
	
	def update_image_change(self, image, changes, filepath):
		if filepath == None or len(filepath) < 1:
			filepath = '<none>'
		if self.show_details:
			print "%d areas changed: %s"  % (len(changes), filepath)
		if (len(changes) > 0):
			draw = ImageDraw.Draw(image)
			
		for area in changes:
			print "%s: %s" % (area.imagearea.name, area.imagearea.shape.get_bounds())
			x, y, w, h = area.imagearea.shape.get_bounds()
			draw.line((x+w, y, x+w, y+h), fill="green", width=3)
			draw.line((x, y+h, x+w, y+h), fill="green", width=3)
			draw.line((x, y, x, y+h), fill="green", width=3)	
		
		
default_diff_vals={'resize_height': 0, 'min_area': 8*7,
				   'threshold': 20, 'kernel_dims': 15, }


class ImageSequencer(object):

	def __init__(self, homedir, use_default=False, show_details=False, save_changes=False):
		self.use_default = use_default
		self.show_details = show_details
		self.log_details = True
		self.imagemap = None
		self.use_imagemaps = False
		self.homedir = homedir
		self.log_fh = open(os.path.join(homedir, 'imagediff.log.txt'), 'w')
		self.changed_dir = os.path.join(homedir, 'changed') if save_changes else None
		if save_changes and not os.path.isdir(self.changed_dir):
			os.makedirs(self.changed_dir)
		'''TODO: We need to be able to adjust DefaultEntropyThreshold
		   in class Entropy_ImageDiffer()
		self.imagediff = Entropy_ImageDiffer(defaultEntropyThreshold)
		self.use_imagemaps = True
		'''
		self.imagediff = Contour_ImageDiff(default_diff_vals)
		self.imagediff.log_fh = self.log_fh
		self.diff_processor = ImageChangeProcessor(self.changed_dir,
												   self.show_details)
		self.diff_processor.log_fh = self.log_fh
		'''
		I see little utility in area-wise  handling when we generally go
		through all the areas for any image...
		self.imagediff.add_area_change_listener \
			(self.diff_processor.update_area_change)
		'''
		self.imagediff.add_image_change_listener \
			(self.diff_processor.update_image_change)
		self.imagedir = os.path.join(homedir, 'images')
		if self.use_imagemaps:
			self.update_dir(self.homedir, self.use_default)

		
	def initialize(self):
		myname = self.__class__.__name__
		self.log("%s.initialize" % myname)
		self.log("%s.homedir %s" % (myname, self.homedir))
		self.log("%s.imagedir %s" % (myname, self.imagedir))
		self.log("%s.changed_dir %s" % (myname, self.changed_dir))
		
	def set_log_details(self, bval):
		self.log_details = bval
		self.diff_processor.set_log_details(bval)
		
	def log(self, txt):
		if self.log_fh:
			self.log_fh.write(txt+'\n')
		print txt
		
	def add_area_change_listener(self, listener):
		self.imagediff.add_area_change_listener(listener)
		
	def add_image_change_listener(self, listener):
		self.imagediff.add_image_change_listener(listener)
		
	def update_image(self, image=None, filepath=None):
		cv2image = self.imagediff.update_image(image)
		return cv2image
		#EntropyImageDiffer:
		#self.imagediff.update_image(image, filepath)
			
	def update_dir(self, imagedir, use_default=False):
		"""
		@imagedir	 directory where we look for imagemaps
		@use_default Only look for default imagemap filename, don't search
		"""
		# imageViewer will open a dir of images while our design has imagemaps
		# one level above that, so we make a *guess* about where imagemaps are
		dirpath = os.path.normpath(imagedir)
		if dirpath.endswith(os.path.sep + 'images'):
			dirpath = imagedir[:-7]
		imap_files = imagemap.find_imagemap_files(dirpath)
		if len(imap_files) < 1:
			tkMessageBox.showinfo("ImageMap Not Found",
				"There are no image maps found in\n%s" % dirpath)
		elif len(imap_files) > 1 and not use_default:
			if debug: print "reading %d imagemap files" % len(imap_files)
			self.imagemap = imagemap.read_imagemaps(dirpath)
		else:
			self.imagemap = imagemap.read_imagemap(dirpath)
		self.imagediff.update_imagemap(self.imagemap)
		if debug:
			print "Area Names: {\n%s\n} // Area Names" % \
				"\n".join([ "  %s" % name for name in self.imagemap.area_names()])

	def get_changes(self):
		return self.diff_processor.get_changes()


class ImageDiffEditor(ImageSequencer):
	
	def __init__(self, tkParent, homedir, use_default=False, show_details=False):
		super(ImageDiffEditor, self).__init__(homedir, use_default, show_details)
		tkParent.title("Image Diff Editor")
		imageseq = fileutil.FileSequence(self.imagedir, Config.image_extensions)
		dialog = ImageSequenceViewer \
			(tkParent, imageseq, 1400, 800,
			 # advance=True, # AutoAdv button and mSecs
			 srcdir=True,	 # "SrcDir" button
			 # destdir=True, # "DestDir" button
			 filecnt=True,	 # "File Index: (n) of (len)"
			 # delete=True,	 # Delete button
			 prevnext=True,	 # Previous and Next buttons
			 # snapsel=True, # "Snap Next", "Quit", "OK" buttons
			 ) # ImageSequenceViewer
		
		dialog.add_update_image_listener(self.update_image)
		dialog.add_sourcedir_change_listener(self.update_dir, self.use_default)
		dialog.setImage(0)
		self.initialize()
		dialog.mainloop()


class ImageDiffSequencer(ImageSequencer):
	
	def __init__(self, homedir, use_default=False, show_details=False):
		super(ImageDiffSequencer, self).__init__(homedir, use_default, show_details)
		self.imageseq = fileutil.FileSequence(self.imagedir, Config.image_extensions)
		self.initialize()
		
	def run(self):
		while True:
			imagefile = self.imageseq.next()
			if imagefile:
				# PIL: img = Image.open(imagefile)
				img = cv2.imread(imagefile)
				self.imagediff.update_image(img, self.imageseq.file())
			else:
				break
		if debug:
			dump_changes(self.diff_processor.get_changes())


def dump_changes(changes):
	for key in sorted(changes.keys()):
		print "imageArea: %s" % key
		for subkey in sorted(changes[key]):
			print "	 image: %s" % subkey
			for change in changes[key][subkey]:
				print "	   %s" % change
			
def errorExit(msg=None):
	msgtxt = '' if msg == None else "%s\n" % msg
	sys.stderr.write("%s%s\n" % (msgtxt, USAGE))
	sys.exit(1)
		
if __name__ == '__main__': 
	homedir, mode = "./test/", ""
	useViewer, useSequencer, show_details = False, False, True

	if len(sys.argv) > 3 and sys.argv[3] in ('True', 'true', '1'):
		show_details = True
	if len(sys.argv) > 2:
		homedir = sys.argv[2]
		mode = sys.argv[1]
		if 'viewer' in mode:
			useViewer = True
		if 'sequencer' in mode:
			useSequencer = True
	else:
		errorExit()

	if not os.path.isdir(homedir):
		errorExit("not a directory: %s" % homedir)
	
	if (useViewer):
		use_default = False
		root = Tk()
		editor = ImageDiffEditor(root, homedir, use_default, show_details)
	elif (useSequencer):
		use_default = True
		sequencer = ImageDiffSequencer(homedir, use_default, show_details)
		sequencer.run()
	else:
		errorExit("not a valid mode: %s" % mode)

