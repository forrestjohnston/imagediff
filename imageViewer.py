from Tkinter import *					# get base widget set
import tkFileDialog
from PIL import Image, ImageTk
import os
import util.fileutil as fileutil
import util.imaging as imaging
import cv2, numpy

from appcfg import Config
"""
	class ImageViewer shows a window with an image scaled to window size
	class ImageSequenceViewer shows ImageViewer with navigation controls
"""
def error(txt): # TODO: Implement error logging!
	sys.stderr.write(txt + '\n')
		
def convert_CV2_to_PIL(image):
	# Rearrange the color channel
	print image.__class__.__name__
	b,g,r = cv2.split(image)
	img = cv2.merge((r,g,b))
	# Convert the Image object into a TkPhoto object
	pil_image = Image.fromarray(img)
	return pil_image

class ImageSource(object):
	
	def __init__(self, id, name, source):
		self.id		= id
		self.name	= name
		self.source = source
		
	def __unicode__(self):
		return "%s, %s, %s" % (self.id, self.name, self.source)


class ImageViewer(Frame):
	def __init__(self, parent=None, img=None, width=640, height=480):
		Frame.__init__(self, parent)
		self.master.title("Image Viewer")
		self.pack(expand=YES, fill=BOTH)
		
		# self.tkImage = None
		self.image	= None
		self.width	= width
		self.height = height
		self.canvas = Canvas(self, relief=SUNKEN)
		self.canvas.config(width=640, height=480)
		#self.canvas.config(scrollregion=(0,0,1000, 1000))
		#self.canvas.configure(scrollregion=self.canvas.bbox('all'))
		self.canvas.config(highlightthickness=0)
		
		sbarV = Scrollbar(self, orient=VERTICAL)
		sbarH = Scrollbar(self, orient=HORIZONTAL)
		
		sbarV.config(command=self.canvas.yview)
		sbarH.config(command=self.canvas.xview)
		
		self.canvas.config(yscrollcommand=sbarV.set)
		self.canvas.config(xscrollcommand=sbarH.set)
		
		sbarV.pack(side=RIGHT, fill=Y)
		sbarH.pack(side=BOTTOM, fill=X)
		
		self.canvas.pack(side=LEFT, expand=YES, fill=BOTH)
		self.update_image(img)
		
	def update_image(self, image=None, display=False):
		if image is None:
			# PIL: self.image = Image.new("RGB", (self.width, self.height), 0)
			self.image = numpy.zeros((self.width, self.height, 3), numpy.uint8)
		elif isinstance(image, basestring): # assume string is filename
			# PIL: self.image = Image.open(image)
			self.image = cv2.imread(image)
		else: # assume numpy/cv2
			self.image = image
		# self.tkImage = imaging.scaledTkImage(self.image, self.width, self.height)
		# self.imageLabel.configure(image=self.tkImage)
		if display:
			self.update_display(self.image)
		return self.image
		
	def update_display(self, image):
		height, width, channels = image.shape
		self.canvas.config(scrollregion=(0, 0, width, height))
		self.photoimg=ImageTk.PhotoImage(convert_CV2_to_PIL(image))
		itemid = self.canvas.create_image(0, 0, anchor="nw", image=self.photoimg)
		pass
	
	
class ImageSequenceViewer(ImageViewer):
	DefaultMSecs = 1000
	MAX_IMAGENAME_DIGITS = 5
	
	def __init__(self, parent=None, sequencer=None, width=640, height=480, **kwargs):
		ImageViewer.__init__(self, parent, None, width, height)
		bottomframe = Frame(parent, bd=8)
		bottomframe.pack(side=BOTTOM)
		self.msecs = ImageSequenceViewer.DefaultMSecs
		self.parent = parent
		self.imagesource = sequencer
		self.image_update_preprocesors = list()
		self.image_update_listeners = list()
		self.sourcedir_change_listeners = list()
		self.loop = 0
		self.editor = None
		self.pack() # Setup the GUI
		
		self.advance	= kwargs.get('advance', False)
		self.cyclefiles = kwargs.get('cyclefiles', False) # last.next is first, first.prev is last
		self.filecnt	= kwargs.get('filecnt', False)	  # "File Index: (n) of (len)"
		self.delete		= kwargs.get('delete', False)	  # Delete button
		self.prevnext	= kwargs.get('prevnext', False)	  # Previous and Next buttons
		self.srcdir		= kwargs.get('srcdir', False)	  # "SrcDir" button
		
		if self.prevnext:
			# Previous/Next Image Navigation Buttons
			Button(bottomframe, text="Next -->", command=self.next).pack(side=RIGHT)
			Button(bottomframe, text="<-- Prev", command=self.prev).pack(side=RIGHT)

		if self.filecnt:
			# File Index 
			count = self.imagesource.count()
			self.fileCountLabel = Label(bottomframe, text="of "+str(count-1))
			self.fileCountLabel.pack(side=RIGHT)
			self.indexEntry = Entry(bottomframe, width=self.MAX_IMAGENAME_DIGITS)
			self.indexEntry.bind("<Return>",self.indexChange)
			self.indexEntry.bind("<Tab>",self.indexChange)
			self.indexEntry.pack(side=RIGHT)
			Label(bottomframe, text="  File Index ").pack(side=RIGHT)
		
		if self.srcdir:
			# Open Directory Button
			self.srcdirButton = Button(bottomframe, text="SourceDir", command=self.sourceDir)
			self.srcdirButton.pack(side=LEFT)
			self.srcdirButton.enabled = False
		
		if self.delete:
			Button(bottomframe, text="Delete",		command=self.onDelete).pack(side=LEFT)
			
		if self.advance:
			# Auto-Advance Controls
			self.autoadvButton = Button(bottomframe, text="AutoAdv", command=self.autoadv)
			self.autoadvButton.pack(side=LEFT)
			self.autoadvButton.enabled = False
			Label(bottomframe, text="mSecs").pack(side=LEFT)
			self.msecEntry = Entry(self, width=5)
			self.msecEntry.pack(side=LEFT)
			self.msecEntry.insert(0, str(self.msecs))
			self.cycleValue = IntVar()
			self.cycleCheckbox = Checkbutton(bottomframe, text='Continuous Cycle',variable=self.cycleValue)
			self.cycleCheckbox.pack(side=LEFT)
		
		# File Name
		self.filenameLabel = Label(bottomframe, text="<filename.jpg>")
		self.filenameLabel.pack(side=RIGHT)
		Label(bottomframe, text="File: ").pack(side=RIGHT)
 
	def setSequencer(self, sequencer):
		self.imagesource = sequencer
		image = self.imagesource.get(0) # Get the first image
		if not image:
			image = Image.new("RGB", (640,480), 0)
		#self.tkImage = utils.scaledTkImage(pilImage(image), self.width, self.height)
		# UNTESTED 2015-01-25
		self.update_image(image)
		
	def setEditor(self, callback):
		self.editor = callback
		
	def add_update_image_preprocessor(self, callback):
		self.image_update_preprocessors.append(callback)
		
	def add_update_image_listener(self, callback):
		self.image_update_listeners.append(callback)
		
	def add_sourcedir_change_listener(self, callback, parm=None):
		self.sourcedir_change_listeners.append((callback, parm))
		
	def indexChange(self, event):
		if self.filecnt:
			try:
				val = int(self.indexEntry.get())
				self.setImage(val)
			except Exception, e:
				print "Error: %s" % e # TODO: Implement logging!
				self.update_labels()
	
	def setSequence(self, seq):
		ImageViewer.setSequencer(self, seq)
		imgobj = self.imagesource.next()
		self.update_image(imgobj)
		self.update_labels()
		
	def getDirDialog(self, titleText):
		return tkFileDialog.askdirectory(parent=self.parent,
										 initialdir=Config.default_dir,
										 title=titleText)
		
	def sourceDir(self):
		dirpath = self.getDirDialog('Please select image source directory')
		if len(dirpath) > 0:
			assert os.path.isdir(dirpath), \
				"sourceDir() not a directory: '%s'" % dirpath
			# self.checkSave() TODO: implement in class that extends ImageSequenceViewer
			if self.editor:
				self.editor.setDir(dirpath)
			self.imagesource = fileutil.FileSequence(dirpath, Config.image_extensions)
			self.update_image(self.imagesource.file(0))
			self.update_sourcedir_change_listeners(dirpath)
		
	def destDir(self):
		dirpath = self.getDirDialog('Please select image destination directory')
		if len(dirpath) > 0:
			# self.checkSave() TODO: implement in class that extends ImageSequenceViewer
			if self.editor:
				self.editor.setDir(dirpath)
			if self.imagesource:
				self.imagesource.refresh(dirpath)
			self.update_image(self.imagesource.file(0))
		
	def onDelete(self):
		assert False, "Not Implemented onDelete()"
		if self.editor:
			self.update_image(self.editor.deleteLast())
		
	def updateMSecs(self):
		try:
			val = int(self.msecEntry.get())
		except Exception, e:
			print "Error: %s" % e # TODO: Implement logging!
			self.msecEntry.delete(0, END)
			self.msecEntry.insert(0, str(self.msecs))
			self.msecs = ImageSequenceViewer.DefaultMSecs
		else:
			self.msecs = val
		return self.msecs
	
	def autoadv(self, enabled=None):
		if enabled or self.autoadvButton.enabled:
			self.autoadvButton.configure(fg="black", text="AutoAdv")
			self.autoadvButton.enabled = False
			self.loop = 0
		else:
			self.updateMSecs()
			self.autoadvButton.configure(fg="red", text="StopAdv")
			self.autoadvButton.enabled = True
			self.loop = 1
			self.onTimer()
	def onTimer(self):
		if self.loop:
			self.next()
			self.after(self.msecs, self.onTimer)
			
	def next(self):
		# if autoAdvance and contCycle off and end-of-imagelist,
		# then stop timer and ignore next
		if (self.advance and self.loop and self.cycleValue.get() == 0 and
			(self.imagesource.index + 1) == self.imagesource.count()):
			self.autoadv() # Stop timer
		else:
			imgobj = self.imagesource.next()
			if imgobj is None:
				if self.cyclefiles:
					imgobj = self.imagesource.file(0)
			else:
				self.update_image(imgobj)
				self.update_labels()
			
	def prev(self):
		imgobj = self.imagesource.prev()
		if imgobj is None and self.cyclefiles:
			imgobj = self.imagesource.last()
		self.update_image(imgobj)
		self.update_labels()
		
	def setImage(self, index):
		imgobj = self.imagesource.file(index)
		self.update_image(imgobj)
		self.update_labels()
		
	def update_labels(self):
		self.updateFilename()
		self.updateIndex()
		
	def updateFilename(self):
		fname = self.imagesource.file()
		if fname:
			self.filenameLabel.config(text=os.path.basename(fname))
	
	def updateIndex(self):
		idx = self.imagesource.index
		if self.filecnt:
			self.indexEntry.delete(0, END)
			self.indexEntry.insert(0, str(idx))
			self.fileCountLabel.config(text="of " +
									   str(self.imagesource.count()-1))
		
	def update_image_preprocessors(self, image, filepath=None):
		try: self.image_update_preprocessors  # See "catch..." note above
		except AttributeError: 
			return
		for listener in self.image_update_preprocessors:
			listener(image, filepath)
			pass
		
	def update_image_listeners(self, image, filepath=None):
		rval = None
		try: 
			self.image_update_listeners  # See "catch..." note above
		except AttributeError: 
			return
		for listener in self.image_update_listeners:
			rval = listener(image, filepath)
		return rval
		
	def update_sourcedir_change_listeners(self, dirpath):
		try: self.sourcedir_change_listeners  # See "catch..." note above
		except AttributeError: 
			return
		for listener, parm in self.sourcedir_change_listeners:
			listener(dirpath, parm)
		
	def update_image(self, imagefile):
		image = cv2.imread(imagefile) \
				if isinstance(imagefile, basestring) else None
		image = ImageViewer.update_image(self, image)
		if imagefile is not None:
			image = self.update_image_listeners(image, imagefile)
		ImageViewer.update_display(self, image)
		

class ImageSequencer(object):
	def __init__(self, imageSequence):
		self.changed  = False
		self.dir	  = None
		self.imageseq = imageSequence
		self.source	  = None
		self.index	  = 0	#  strictly for filenames
		pass
	
	def getDir(self):
		return self.dir
	
	def getFilelist(self):
		return self.imageseq
	
	def setImageSource(self, source=None):
		self.source = source
		
	def setDir(self, dir):
		assert os.path.isdir(dir)
		self.dir = dir
		self.imageseq.refresh(dir)
		self.changed = True
		# self.index = 0  # was: last element
		# get the last image and update UI
		return self.imageseq.file(0) # was: get last element
	
	def deleteLast(self):
		assert False, "Not Implemented deleteLast()"
		return self.imageseq.deleteLast()
	
	def next(self):
		"""Return the next image.
		Raises exception on source error"""
		rval = None
		if self.dir and os.path.isdir(self.dir):
			if self.source is None:
				self.source = ImageSource(self.dir)
			filename = "%s/%05d.jpg"%(self.dir, self.index)
			self.source.next(filename)
			self.imageseq.append(filename)
			self.index += 1
			rval = filename
		return rval
	
	def update_image(self, imagefile):
		print "ImageSequencer.update_image(%s)"%(imagefile)
		pass
	
# ===========================================================================
'''
def test_ImageViewer(parent, files):
	parent.title("Scaled Image Viewer")
	sequence = ImageFileSequence(files)
	image = sequence.next()
	dialog = ImageViewer(image, parent, 960, 540)
	dialog.mainloop()
		
'''
def test_ImageSequenceViewer(parent, homedir):
	imagedir = os.path.join(homedir, 'images')
	parent.title("Image Sequence Viewer")
	imageseq = fileutil.FileSequence(imagedir, Config.image_extensions)
	dialog = ImageSequenceViewer(parent, imageseq, 960, 540,
								 #advance=True,	 # AutoAdv button and mSecs
								 srcdir=True,  # "SrcDir" button
								 filecnt=True,	# "File Index: (n) of (len)"
								 delete=True,	# Delete button
								 prevnext=True, # Previous and Next buttons
								 ) # ImageSequenceViewer
	dialog.setEditor(ImageSequencer(imageseq))
	dialog.setImage(0)
	dialog.mainloop()
	
	
if __name__ == '__main__': 
	root = Tk()
	dir = "./test/data/singlemap"
	#test_ImageViewer(root, files)
	test_ImageSequenceViewer(root, dir)
	#test_SnapSelectQuit(root, dir)

