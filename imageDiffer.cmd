@echo OFF
if {%2} == {} goto usage
if {%1} == {viewer}    set mode=viewer
if {%1} == {sequencer} set mode=sequencer
if {%mode%} == {} GOTO :usage
IF EXIST %2 GOTO :options_set
:usage
echo USAGE: imageDiffer ^<viewer^|sequencer^> ^<dir_of_imagemaps^>
goto :EOF
:options_set

set workspace=C:\forrest\workspace
set PYTHONPATH=%PYTHONPATH%;%workspace%\utils;
call python.exe %workspace%\imagediff\imagediff.py %mode% %2

:EOF